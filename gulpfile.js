var path = 'app';

var gulp = require('gulp');
var uglify = require('gulp-uglify');
var compass = require('gulp-compass');
var concat = require('gulp-concat');
var del = require('del');
var runSequence = require('run-sequence');
var uncss = require('gulp-uncss');


gulp.task('uncss', function () {
    return gulp.src('temp/style-not-used.css')
        .pipe(uncss({
            html: ['index.html']
        }))
        .pipe(gulp.dest('./out'));
});

gulp.task('sass', function() {
    gulp.src('./sass/**/*.scss')
    .pipe(compass({
        config_file: 'config.rb',
    }))
    .on('error', function(error) {
        console.log(error);
        this.emit('end');
    })
    .pipe(gulp.dest('temp'));
});

gulp.task('clean-lib', function(callback) {
   return  del(['./'+path+'/js/min'], callback);
});

gulp.task('js', ['clean-lib'], function() {
    gulp.src([
        path+'/assets/js/bootstrap.js',
        path+'/assets/js/parallax.js',
        path+'/assets/js/static.js'
        ])
    .pipe(concat('app.js'))
    .pipe(gulp.dest(path+'/assets/js/min'));

     gulp.src([
        path+'/assets/js/jquery.min.js'
        ])
    .pipe(concat('jquery.js'))
    .pipe(gulp.dest(path+'/assets/js/min'));

});

gulp.task('watch', function() {
    gulp.watch(path+'/assets/sass/**/*.scss', ['sass']);
    console.log('watching.. press ctrl + c to leave.');
});

gulp.task('clean-release', function(callback) {
    return  del(['release/**/*'], callback);
});

gulp.task('move', function() { 
    gulp.src([
        path+'/**/*',
        '!'+path+'/assets/{sass,sass/**}',
        '!'+path+'/assets/js/*.js'
        ]).pipe(gulp.dest('release'));
});

gulp.task('release', function(callback) {
    runSequence('clean-release', 'sass', 'move', callback);
});



gulp.task('default', ['sass']);
