# includes

# path to source files
project_path = "./app/assets"

# no path added to assets by default
http_path = "./"

# have to match gulp-compass settings
sass_dir = "sass"
css_dir = "css"

#relative_assets = false
line_comments = false
#output_style = :expanded
output_style = :compressed

