$(document).ready(function() {
    var e = $("body"),
        t = $(window),
        i = $(".main-cta-btn").length > 0 ? $(".main-cta-btn").offset().top : 150;
    t.scroll(function() {
        var t = $(document).scrollTop(),
            n = $("#nav.navbar-fixed-top"),
            s = $(".section-nav"),
            a = s.closest(".section-nav-wrap"),
            o = s.hasClass("sticky"),
            r = $(".sticky-footer");
        t >= i ? (e.addClass("navbar-small"), n.addClass("navbar-small"), r.addClass("active")) : (e.removeClass("navbar-small"), n.removeClass("navbar-small"), r.removeClass("active")), o && (UC.utils.viewportWidth() >= 768 && t >= a.offset().top ? (s.addClass("fixed"), $(".nav-section").each(function(e) {
            $(this).position().top <= t + 100 && ($(".section-nav-tab.active").removeClass("active"), $(".section-nav-tab").eq(e).addClass("active"))
        }), r.addClass("active")) : (s.removeClass("fixed"), $(".section-nav-tab.active").removeClass("active"), $(".section-nav-tab:first").addClass("active")))
    }).scroll()
});
